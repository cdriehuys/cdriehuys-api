import os

from django.core.exceptions import ImproperlyConfigured

import requests


SECRET_KEY = os.environ.get('SECRET_KEY')

if SECRET_KEY is None:
    raise ImproperlyConfigured("The 'SECRET_KEY' environment variable must be "
                               "set.")


# Only set debug mode if the DEBUG environment variable is true.

debug_val = os.environ.get('DEBUG')

if debug_val is not None:
    DEBUG = debug_val.lower() == 'true'
else:
    DEBUG = False


ALLOWED_HOSTS = [
    'localhost',
]

# If a custom domain is used, add it to the allowed hosts

custom_domain = os.environ.get('CUSTOM_DOMAIN')

if custom_domain is not None:
    ALLOWED_HOSTS.append(custom_domain)

# Allow requests from the EC2 instances in the environment
# Solution from: http://stackoverflow.com/a/33527496/3762084

try:
    EC2_IP = requests.get(
        'http://169.254.169.254/latest/meta-data/local-ipv4').text
    ALLOWED_HOSTS.append(EC2_IP)
except requests.exceptions.RequestException:
    pass


# Set up Database with RDS Credentials

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['RDS_DB_NAME'],
        'USER': os.environ['RDS_USERNAME'],
        'PASSWORD': os.environ['RDS_PASSWORD'],
        'HOST': os.environ['RDS_HOSTNAME'],
        'PORT': os.environ['RDS_PORT'],
    }
}
