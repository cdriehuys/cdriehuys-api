"""cdriehuys_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin

from rest_framework_jwt import views as token_views


auth_views = [
    url(r'^token/$', token_views.obtain_jwt_token, name='get-jwt'),
    url(r'^token/refresh/$', token_views.refresh_jwt_token,
        name='refresh-jwt'),
    url(r'^', include('rest_framework.urls', namespace='rest_framework')),
]


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include(auth_views)),
    url(r'^todo/', include('todo_api.urls')),
]
